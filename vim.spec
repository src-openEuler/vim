%{!?_with_selinux__:%global _with_selinux__ 1}
%{!?_with_ruby__:%define _with_ruby__ 1}
%{!?_with_lua__:%define _with_lua__ 1}
%{!?_with_netbeans__:%define _with_netbeans__ 1}

%define baseversion 9.1
%define patchlevel 1166
%define vimdir vim91

Name:           vim
Epoch:          2
Version:        %{baseversion}.%{patchlevel}
Release:        1
Summary:        Vim is a highly configurable text editor for efficiently creating and changing any kind of text.
License:        Vim AND LGPL-2.1-or-later AND MIT AND GPL-1.0-only AND (GPL-2.0-only OR Vim) AND Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND GPL-2.0-or-later AND GPL-3.0-or-later AND OPUBL-1.0 AND Apache-2.0 WITH Swift-exception
URL:            https://www.vim.org
Source0:        https://github.com/vim/vim/archive/refs/tags/v%{version}.tar.gz
Source1:        virc
Source2:        vimrc

Patch0000:      vim-7.0-fixkeys.patch
Patch0001:      vim-9.1-specsyntax.patch
Patch0002:      vim-8.0-copy-paste.patch
Patch0003:      vim-python3-tests.patch
Patch0005:      bugfix-rm-modify-info-version.patch
Patch0006:      vim-Add-sw64-architecture-debcontrol.patch

BuildRequires:  autoconf python3-devel ncurses-devel gettext perl-devel perl-generators gcc
BuildRequires:  perl(ExtUtils::Embed) perl(ExtUtils::ParseXS) libacl-devel gpm-devel file
BuildRequires:  pkgconfig(libsodium)
BuildRequires:  desktop-file-utils >= 0.2.93 libtool make chrpath
%if %{_with_selinux__}
BuildRequires:  libselinux-devel
%endif
%if %{_with_ruby__}
BuildRequires:  ruby-devel ruby
%endif
%if %{_with_lua__}
BuildRequires:  lua-devel
%endif
Requires:       desktop-file-utils
Conflicts:      filesystem < 3

%description
Vim is an advanced text editor that seeks to provide the power of the
de-facto Unix editor 'Vi', with a more complete feature set. Vim is a
highly configurable text editor built to enable efficient text editing.
It is an improved version of the vi editor distributed with most UNIX
systems.

%package common
Summary:        This contains some common files to use vim editor.
Requires:       %{name}-filesystem
Conflicts:      man-pages-fr < 0.9.7-14 man-pages-it < 0.3.0-17 man-pages-pl < 0.24-2 %{name}-minimal < 8.0.1428-4

%description common
This common package contains files that will be needed when you run vim editor. This is also needed by vim-enhaned and vim-X11 packages.

%package minimal
Summary:        This package provides the basic and minimal functionalities of vim editor.
Provides:       vi = %{version}-%{release} %{_bindir}/vi
Conflicts:      %{name}-common < 8.0.1428-4

%description minimal
The minimal package provides a minimal version of vim editor. It will be installed as /usr/bin/vi.

%package enhanced
Summary:        This is a package containing enhanced vim editor.
Requires:       vim-common = %{epoch}:%{version}-%{release} which
Provides:       vim = %{version}-%{release} %{_bindir}/mergetool %{_bindir}/vim
Suggests:       python3 python3-libS
Suggests:       perl-libs perl-devel
%if %{_with_ruby__}
Suggests:       ruby-libs ruby
%endif
%if %{_with_lua__}
Suggests:       lua-libs
%endif

%description enhanced
This packages includes some enhancements for the vim editor.

%package filesystem
Summary:        The vim filesystem.
BuildArch:      noarch

%description filesystem
This package contains the vim filesystem.

%package X11
Summary:        Vim for the X Window System i.e.gvim
BuildRequires:  gtk3-devel libX11-devel libSM-devel libXt-devel libXpm-devel libICE-devel libappstream-glib
Requires:       vim-common = %{epoch}:%{version}-%{release} libattr >= 2.4 hicolor-icon-theme
Provides:       gvim = %{version}-%{release} %{_bindir}/mergetool %{_bindir}/gvim
Suggests:       python3 python3-libs
Suggests:       perl-libs perl-devel
%if %{_with_ruby__}
Suggests:       ruby-libs ruby
%endif
%if %{_with_lua__}
Suggests:       lua-libs
%endif

%description X11
This X11 package serves you the ability to use vim with graphics and mouse.

%prep
%autosetup -n %{name}-%{baseversion}.%{patchlevel} -p1

#ipv6 test fail in CI, it should be related to the ipv6 configuration on jenkins, which is successful on openEuler obs
rm -rf src/testdir/test_channel.*

%build
%define _make_cmd__() %{make_build} VIMRCLOC=/etc VIMRUNTIMEDIR=/usr/share/vim/%{vimdir}; cp vim %{?1}; %{!?2:make clean}

export CFLAGS="%{optflags} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_FORTIFY_SOURCE=2 -D__linux__ -D_REENTRANT"
export CXXFLAGS="%{optflags} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_FORTIFY_SOURCE=2 -D__linux__ -D_REENTRANT"

sed -i 's/nawk/awk/g' runtime/tools/mve.awk
cd src; autoconf

%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/virc\"'" \
 --with-features=small --with-x=no --enable-multibyte --disable-netbeans --disable-libsodium \
 --disable-pythoninterp --disable-perlinterp --disable-tclinterp --with-tlib=ncurses \
 --enable-gui=no --disable-gpm --exec-prefix=/ \
 --enable-fail-if-missing --with-python3-config-dir=/usr/lib64/python%{python3_version}/config-%{python3_version}-%{_arch}-linux-gnu \
%if %{_with_selinux__}
 --enable-selinux \
%else
 --disable-selinux \
%endif

%{_make_cmd__ vim-minimal}

%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/vimrc\"'" \
 --with-features=huge --enable-python3interp=dynamic --enable-libsodium \
 --enable-perlinterp=dynamic --disable-tclinterp --with-x=yes --enable-xim --enable-multibyte \
 --with-tlib=ncurses --enable-gtk3-check --enable-gui=gtk3 \
 --enable-cscope --enable-fail-if-missing --with-python3-config-dir=/usr/lib64/python%{python3_version}/config-%{python3_version}-%{_arch}-linux-gnu \
%if %{_with_netbeans__}
 --enable-netbeans \
%else
 --disable-netbeans \
%endif
%if %{_with_selinux__}
 --enable-selinux \
%else
 --disable-selinux \
%endif
%if %{_with_ruby__}
 --enable-rubyinterp=dynamic \
%else
 --disable-rubyinterp \
%endif
%if %{_with_lua__}
 --enable-luainterp=dynamic \
%else
 --disable-luainterp \
%endif

%{_make_cmd__ vim-X11}

%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/vimrc\"'" \
 --with-features=huge --enable-python3interp=dynamic --enable-libsodium \
 --enable-perlinterp=dynamic --disable-tclinterp --with-x=no --enable-gui=no --enable-multibyte \
 --enable-cscope --with-tlib=ncurses --with-python3-config-dir=/usr/lib64/python%{python3_version}/config-%{python3_version}-%{_arch}-linux-gnu \
 --enable-fail-if-missing \
%if %{_with_netbeans__}
 --enable-netbeans \
%else
 --disable-netbeans \
%endif
%if %{_with_selinux__}
 --enable-selinux \
%else
 --disable-selinux \
%endif
%if %{_with_ruby__}
 --enable-rubyinterp=dynamic \
%else
 --disable-rubyinterp \
%endif
%if %{_with_lua__}
 --enable-luainterp=dynamic \
%else
 --disable-luainterp \
%endif

%{_make_cmd__ vim-enhanced noclean}

%install
pushd src
%make_install BINDIR=%{_bindir} VIMRCLOC=%{_sysconfdir} VIMRUNTIMEDIR=%{_datadir}/%{name}/%{vimdir}
make installgtutorbin  DESTDIR=%{buildroot} BINDIR=%{_bindir} VIMRCLOC=%{_sysconfdir} VIMRUNTIMEDIR=%{_datadir}/%{name}/%{vimdir}

install -m755 vim-minimal %{buildroot}%{_bindir}/vi
install -m755 vim-enhanced %{buildroot}%{_bindir}/vim
install -m755 vim-X11 %{buildroot}%{_bindir}/gvim
popd

rm -f README*.info

for dir in {autoload,colors,compiler,doc,ftdetect,ftplugin,indent,keymap,lang,plugin,print,spell,syntax,tutor}
do
    install -d %{buildroot}%{_datadir}/%{name}/vimfiles/${dir}
    install -d %{buildroot}%{_datadir}/%{name}/vimfiles/after/${dir}
done

install -m644 %{_builddir}/%{name}-%{baseversion}.%{patchlevel}/runtime/doc/uganda.txt %{_builddir}/%{name}-%{baseversion}.%{patchlevel}/LICENSE

install -d %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps

install -d %{buildroot}%{_metainfodir}
cat > %{buildroot}%{_metainfodir}/gvim.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2014 Richard Hughes <richard@hughsie.com> -->
<!--
EmailAddress: Bram@moolenaar.net>
SentUpstream: 2014-05-22
-->
<component type="desktop-application">
  <id>org.vim.Vim</id>
  <name>GVim</name>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>Vim</project_license>
  <summary>The VIM version of the vi editor for the X Window System</summary>
  <description>
    <p>
     Vim is an advanced text editor that seeks to provide the power of the
     de-facto Unix editor 'Vi', with a more complete feature set.
     It's useful whether you're already using vi or using a different editor.
    </p>
    <p>
     Vim is a highly configurable text editor built to enable efficient text
     editing.
     Vim is often called a "programmer's editor," and so useful for programming
     that many consider it an entire IDE. It is not just for programmers, though.
     Vim is perfect for all kinds of text editing, from composing email to
     editing configuration files.
    </p>
    <p>
     We ship the current Vim stable release - %{baseversion} - with the upstream
     patchlevel %{patchlevel} applied, which is combined into version %{version}
     used during packaging.
    </p>
  </description>
  <releases>
    <release version="%{version}" date="%(date +%F -r %{SOURCE0})" />
  </releases>
  <screenshots>
    <screenshot type="default">
      <image>https://raw.githubusercontent.com/zdohnal/vim/zdohnal-screenshot/gvim16_9.png</image>
    </screenshot>
  </screenshots>
  <url type="homepage">http://www.vim.org/</url>
  <content_rating type="oars-1.1"/>
</component>
EOF

%define _linkvi__ %{buildroot}%{_bindir}/{rvi,rview,view,ex}
%define _linkvim__ %{buildroot}%{_bindir}/{rvim,vimdiff}
%define _linkgvim__ %{buildroot}%{_bindir}/{gview,gex,evim,gvimdiff,vimx}
%define _linkcmds__() for file in %{expand:%{_link%{1}__}};do ln -sf %{1} ${file};done;

%{_linkcmds__ vi}
%{_linkcmds__ vim}
%{_linkcmds__ gvim}

pushd %{buildroot}%{_mandir}/man1
sed -i 's,%{buildroot},,' {vim.1,vimtutor.1}
rm -f rvim.1
install -p vim.1 vi.1
ln -sf vi.1 rvi.1
ln -sf vim.1 vimdiff.1
popd

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{_builddir}/%{name}-%{baseversion}.%{patchlevel}/runtime/gvim.desktop

appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml

install -Dpm644 %{SOURCE1} %{buildroot}%{_sysconfdir}/virc
install -pm644 %{SOURCE2} %{buildroot}%{_sysconfdir}/vimrc

rm -f %{buildroot}/%{_datadir}/vim/%{vimdir}/macros/maze/maze*.c
rm -rf %{buildroot}/%{_datadir}/vim/%{vimdir}/tools
rm -rf %{buildroot}/%{_datadir}/vim/%{vimdir}/doc/vim2html.pl
rm -f %{buildroot}/%{_datadir}/vim/%{vimdir}/tutor/tutor.gr.utf-8~

# Remove not UTF-8 manpages
for i in pl.ISO8859-2 it.ISO8859-1 ru.KOI8-R fr.ISO8859-1 da.ISO8859-1 de.ISO8859-1 tr.ISO8859-9; do
  rm -rf %{buildroot}/%{_mandir}/$i
done

# use common man1/ru directory
mv %{buildroot}%{_mandir}/ru.UTF-8 %{buildroot}%{_mandir}/ru

# Remove duplicate man pages
for i in fr.UTF-8 it.UTF-8 pl.UTF-8 da.UTF-8 de.UTF-8 tr.UTF-8; do
  rm -rf %{buildroot}/%{_mandir}/$i
done

install -d %{buildroot}%{_mandir}/man5
for files in %{buildroot}%{_mandir}/{man1/{rvim.1,gvim.1,gex.1,gview.1,vimx.1},man5/vimrc.5}
do
    echo ".so man1/vim.1" > ${files}
done
echo ".so man1/vimdiff.1" > %{buildroot}%{_mandir}/man1/gvimdiff.1
echo ".so man1/vimtutor.1" > %{buildroot}%{_mandir}/man1/gvimtutor.1
echo ".so man1/vi.1" > %{buildroot}%{_mandir}/man5/virc.5
touch %{buildroot}%{_datadir}/%{name}/vimfiles/doc/tags

chrpath -d %{buildroot}%{_bindir}/vim
chrpath -d %{buildroot}%{_bindir}/xxd
 
pushd runtime
ln -sf ../../%{name}/%{vimdir}/doc docs
popd

%transfiletriggerin common -- %{_datadir}/%{name}/vimfiles/doc
%{_bindir}/vim -c ":helptags %{_datadir}/%{name}/vimfiles/doc" -c :q &> /dev/null || :

%transfiletriggerpostun common -- %{_datadir}/%{name}/vimfiles/doc
> %{_datadir}/%{name}/vimfiles/doc/tags || :
%{_bindir}/vim -c ":helptags %{_datadir}/%{name}/vimfiles/doc" -c :q &> /dev/null || :

%files common
%license LICENSE
%doc README*
%doc runtime/docs
%config(noreplace) %{_sysconfdir}/vimrc
%dir %{_datadir}/%{name}/%{vimdir}
%{_datadir}/%{name}/%{vimdir}/LICENSE
%{_datadir}/%{name}/%{vimdir}/README.txt
%{_datadir}/%{name}/%{vimdir}/{rgb.txt,autoload,colors,compiler,pack,doc}
%{_datadir}/%{name}/%{vimdir}/{*.vim,ftplugin,indent,keymap,macros,plugin}
%{_datadir}/%{name}/%{vimdir}/{print,syntax,tutor,spell}
%{_datadir}/%{name}/%{vimdir}/lang/{*.vim,*.txt}
%{_datadir}/%{name}/%{vimdir}/import/dist/{vimhelp.vim,vimhighlight.vim}
%{_bindir}/xxd
%lang(af) %{_datadir}/%{name}/%{vimdir}/lang/af
%lang(am) %{_datadir}/%{name}/%{vimdir}/lang/am
%lang(ca) %{_datadir}/%{name}/%{vimdir}/lang/ca
%lang(cs) %{_datadir}/%{name}/%{vimdir}/lang/cs
%lang(cs.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/cs.cp1250
%lang(da) %{_datadir}/%{name}/%{vimdir}/lang/da
%lang(de) %{_datadir}/%{name}/%{vimdir}/lang/de
%lang(en_GB) %{_datadir}/%{name}/%{vimdir}/lang/en_GB
%lang(eo) %{_datadir}/%{name}/%{vimdir}/lang/eo
%lang(es) %{_datadir}/%{name}/%{vimdir}/lang/es
%lang(fi) %{_datadir}/%{name}/%{vimdir}/lang/fi
%lang(fr) %{_datadir}/%{name}/%{vimdir}/lang/fr
%lang(ga) %{_datadir}/%{name}/%{vimdir}/lang/ga
%lang(hu) %{_datadir}/%{name}/%{vimdir}/lang/hu
%lang(it) %{_datadir}/%{name}/%{vimdir}/lang/it
%lang(ja) %{_datadir}/%{name}/%{vimdir}/lang/ja
%lang(ja.euc-jp) %{_datadir}/%{name}/%{vimdir}/lang/ja.euc-jp
%lang(ja.sjis) %{_datadir}/%{name}/%{vimdir}/lang/ja.sjis
%lang(ko) %{_datadir}/%{name}/%{vimdir}/lang/ko
%lang(ko) %{_datadir}/%{name}/%{vimdir}/lang/ko.UTF-8
%lang(lv) %{_datadir}/%{name}/%{vimdir}/lang/lv
%lang(nb) %{_datadir}/%{name}/%{vimdir}/lang/nb
%lang(nl) %{_datadir}/%{name}/%{vimdir}/lang/nl
%lang(no) %{_datadir}/%{name}/%{vimdir}/lang/no
%lang(pl) %{_datadir}/%{name}/%{vimdir}/lang/pl
%lang(pl.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/pl.UTF-8
%lang(pl.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/pl.cp1250
%lang(pt_BR) %{_datadir}/%{name}/%{vimdir}/lang/pt_BR
%lang(ru) %{_datadir}/%{name}/%{vimdir}/lang/ru
%lang(ru.cp1251) %{_datadir}/%{name}/%{vimdir}/lang/ru.cp1251
%lang(sk) %{_datadir}/%{name}/%{vimdir}/lang/sk
%lang(sk.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/sk.cp1250
%lang(sr) %{_datadir}/%{name}/%{vimdir}/lang/sr
%lang(sv) %{_datadir}/%{name}/%{vimdir}/lang/sv
%lang(tr) %{_datadir}/%{name}/%{vimdir}/lang/tr
%lang(uk) %{_datadir}/%{name}/%{vimdir}/lang/uk
%lang(uk.cp1251) %{_datadir}/%{name}/%{vimdir}/lang/uk.cp1251
%lang(vi) %{_datadir}/%{name}/%{vimdir}/lang/vi
%lang(zh_CN) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN
%lang(zh_CN.cp936) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN.cp936
%lang(zh_TW) %{_datadir}/%{name}/%{vimdir}/lang/zh_TW
%lang(zh_CN.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN.UTF-8
%lang(zh_TW.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/zh_TW.UTF-8
%lang(fr) %{_mandir}/fr/man1/*
%lang(da) %{_mandir}/da/man1/*
%lang(de) %{_mandir}/de/man1/*
%lang(it) %{_mandir}/it/man1/*
%lang(ja) %{_mandir}/ja/man1/*
%lang(pl) %{_mandir}/pl/man1/*
%lang(ru) %{_mandir}/ru/man1/*
%lang(tr) %{_mandir}/tr/man1/*
%{_mandir}/man1/{gex.*,gview.*,gvim*,rvim.*,vim.*,vimdiff.*}
%{_mandir}/man1/{vimtutor.*,vimx.*,xxd.*}
%{_mandir}/man5/vimrc.*

%files minimal
%config(noreplace) %{_sysconfdir}/virc
%{_bindir}/{ex,vi,view,rvi,rview}
%{_mandir}/man1/{vi.*,ex.*,rvi.*,rview.*,view.*}
%{_mandir}/man5/virc.*
%{_datadir}/%{name}/%{vimdir}/defaults.vim

%files enhanced
%{_bindir}/{vim,rvim,vimdiff,vimtutor}

%files filesystem
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/vimfiles
%dir %{_datadir}/%{name}/vimfiles/after/*
%dir %{_datadir}/%{name}/vimfiles/{autoload,colors,compiler,doc,ftdetect,ftplugin}
%dir %{_datadir}/%{name}/vimfiles/{indent,keymap,lang,plugin,print,spell,syntax,tutor}
%ghost %{_datadir}/%{name}/vimfiles/doc/tags

%files X11
%exclude %{_datadir}/applications/vim.desktop
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/applications/*
%{_datadir}/icons/{hicolor,locolor}/*/apps/*
%{_bindir}/{gvimtutor,gvim,gvimdiff,gview,gex}
%{_bindir}/{vimtutor,vimx,evim}
%{_mandir}/man1/evim.*

%changelog
* Mon Mar 03 2025 Funda Wang <fundawang@yeah.net> - 2:9.1.1166-1
- update to 9.1 patchlevel 1166

* Tue Feb 25 2025 Funda Wang <fundawang@yeah.net> - 2:9.1.1120-1
- update to 9.1 patchlevel 1120
- add sw_64 into debcontrol

* Tue Feb 18 2025 wangjiang <app@cameyan.com> - 2:9.1.1019-3
- fix CVE-2025-1215 CVE-2025-26603

* Mon Jan 20 2025 wangjiang <app@cameyan.com> - 2:9.1.1019-2
- fix CVE-2025-24014

* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 2:9.1.1019-1
- update to 9.1 patchlevel 1019
- disable check section as it hangs too long

* Fri Dec 13 2024 wangjiang <app@cameyan.com> - 2:9.1.0764-2
- fix tiny-Vim crashes with fuzzy buffer completion

* Mon Oct 07 2024 Funda Wang <fundawang@yeah.net> - 2:9.1.0764-1
- update to 9.1 patchlevel 0764

* Mon Sep 02 2024 Funda Wang <fundawang@yeah.net> - 2:9.1.0709-1
- update to 9.1 patchlevel 0709

* Thu Aug 29 2024 wangjiang <app@cameyan.com> - 2:9.0.2092-11
- Type:CVE
- ID:CVE-2024-43802
- SUG:NA
- DESC:fix CVE-2024-43802

* Wed Aug 21 2024 Funda Wang <fundawang@yeah.net> - 2:9.0.2092-10
- Type:enhacement
- ID:NA
- SUG:NA
- DESC:Recognize epoch when making spec changelog

* Fri Aug 16 2024 zhangxianting <zhangxianting@uniontech.com> - 2:9.0.2092-9
- Type:CVE
- ID:CVE-2024-43374
- SUG:NA
- DESC:fix CVE-2024-43374

* Tue Aug 06 2024 wangjiang <wangjiang37@h-partners.com> - 2:9.0.2092-8
- Type:CVE
- ID:CVE-2024-41957 CVE-2024-41965
- SUG:NA
- DESC:fix CVE-2024-41957 CVE-2024-41965

* Mon Jul 29 2024 Funda Wang <fundawang@yeah.net> - 2:9.0.2092-7
- Type:enhacement
- ID:NA
- SUG:NA
- DESC:support newly introduced rpm tags

* Fri Jul 12 2024 wangjiang <wangjiang37@h-partners.com> - 2:9.0.2092-6
- Type:CVE
- ID:CVE-2023-48232
- SUG:NA
- DESC:fix CVE-2023-48232

* Mon Jul 08 2024 wangjiang <wangjiang37@h-partners.com> - 2:9.0.2092-5
- Type:CVE
- ID:CVE-2024-22667
- SUG:NA
- DESC:fix CVE-2024-22667

* Fri Jul 5 2024 Han Jinpeng <hanjinpeng@kylinos.cn> - 2:9.0.2092-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: backport patch, so that vim reads /etc/vimrc at startup

* Sat May 11 2024 wangjiang <wangjiang37@h-partners.com> - 2:9.0.2092-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport upstream patch to fix some bugs

* Wed May 08 2024 yinyongkang <yinyongkang@kylinos.cn>  - 2:9.0.2092-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: overflow with count for :s command

* Sun Feb 04 2024 wangjiang <wangjiang37@h-partners.com> - 2:9.0.2092-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade version to 9.0.2092

* Tue Nov 28 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-42
- Type:CVE
- ID:CVE-2023-48706
- SUG:NA
- DESC:fix CVE-2023-48706

* Wed Nov 22 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-41
- Type:CVE
- ID:CVE-2023-48231 CVE-2023-48233 CVE-2023-48234 CVE-2023-48235 CVE-2023-48236 CVE-2023-48237
- SUG:NA
- DESC:fix CVE-2023-48231 CVE-2023-48233 CVE-2023-48234 CVE-2023-48235 CVE-2023-48236 CVE-2023-48237

* Mon Nov 06 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-40
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix garbled characters display with keywords of filename

* Wed Nov 01 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-39
- Type:CVE
- ID:CVE-2023-46246
- SUG:NA
- DESC:fix CVE-2023-46246

* Mon Oct 16 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-38
- Type:CVE
- ID:CVE-2023-5441 CVE-2023-5535
- SUG:NA
- DESC:fix CVE-2023-5441 CVE-2023-5535

* Tue Oct 10 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-37
- Type:CVE
- ID:CVE-2023-5344
- SUG:NA
- DESC:fix CVE-2023-5344

* Tue Sep 12 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-36
- Type:CVE
- ID:CVE-2023-4738 CVE-2023-4750 CVE-2023-4752 CVE-2023-4781
- SUG:NA
- DESC:fix CVE-2023-4738 CVE-2023-4750 CVE-2023-4752 CVE-2023-4781

* Thu Sep 07 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-35
- Type:CVE
- ID:CVE-2023-4736 CVE-2023-4733 CVE-2023-4734 CVE-2023-4735
- SUG:NA
- DESC:fix CVE-2023-4736 CVE-2023-4733 CVE-2023-4734 CVE-2023-4735

* Mon Aug 07 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-34
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build failed due to Perl upgrade

* Sat May 13 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-33
- Type:CVE
- ID:CVE-2023-2609 CVE-2023-2610
- SUG:NA
- DESC:CVE-2023-2609 CVE-2023-2610

* Thu May 04 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-32
- Type:CVE
- ID:CVE-2023-2426
- SUG:NA
- DESC:CVE-2023-2426

* Fri Mar 24 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-31
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:vim reads /etc/vimrc at startup

* Fri Mar 17 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-30
- Type:CVE
- ID:CVE-2023-1264
- SUG:NA
- DESC:CVE-2023-1264

* Wed Mar 08 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-29
- Type:CVE
- ID:CVE-2023-1170 CVE-2023-1175
- SUG:NA
- DESC:CVE-2023-1170 CVE-2023-1175

* Wed Feb 22 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-28
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport upstream patch to fix memory leak

* Mon Feb 06 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-27
- Type:CVE
- ID:CVE-2023-0433
- SUG:NA
- DESC:CVE-2023-0433

* Sun Jan 29 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-26
- Type:CVE
- ID:CVE-2022-47024 CVE-2023-0288
- SUG:NA
- DESC:CVE-2022-47024 CVE-2023-0288

* Mon Jan 09 2023 wangjiang <wangjiang37@h-partners.com> - 2:9.0-25
- Type:CVE
- ID:CVE-2023-0049 CVE-2023-0051 CVE-2023-0054
- SUG:NA
- DESC:CVE-2023-0049 CVE-2023-0051 CVE-2023-0054

* Mon Dec 12 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-24
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove rpath and runpath of exec files and libraries

* Thu Dec 08 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-23
- Type:CVE
- ID:CVE-2022-4292 CVE-2022-4293
- SUG:NA
- DESC:fix CVE-2022-4292 CVE-2022-4293

* Tue Dec 06 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-22
- Type:CVE
- ID:CVE-2022-3491 CVE-2022-3520 CVE-2022-3591
- SUG:NA
- DESC:fix CVE-2022-3491 CVE-2022-3520 CVE-2022-3591

* Tue Nov 29 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-21
- Type:CVE
- ID:CVE-2022-4141
- SUG:NA
- DESC:fix CVE-2022-4141

* Tue Nov 01 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-20
- Type:CVE
- ID:CVE-2022-3705
- SUG:NA
- DESC:fix CVE-2022-3705

* Mon Oct 17 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:9.0-19
- Type:CVE
- ID:CVE-2022-3278 CVE-2022-3297 CVE-2022-3324
- SUG:NA
- DESC:fix CVE-2022-3278 CVE-2022-3297 CVE-2022-3324

* Wed Oct 12 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:9.0-18
- Type:CVE
- ID:CVE-2022-3352
- SUG:NA
- DESC:fix CVE-2022-3352

* Thu Sep 29 2022 huangduirong <huangduirong@huawei.com> - 2:9.0-17
- Type:CVE
- ID:CVE-2022-3296
- SUG:NA
- DESC:fix CVE-2022-3296

* Fri Sep 23 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:9.0-16
- Type:CVE
- ID:CVE-2022-3256
- SUG:NA
- DESC:fix CVE-2022-3256

* Mon Sep 19 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:9.0-15
- Type:CVE
- ID:CVE-2022-3234 CVE-2022-3235
- SUG:NA
- DESC:fix CVE-2022-3234 CVE-2022-3235

* Fri Sep 16 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-14
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add BuildRequires make

* Tue Sep 13 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-13
- Type:CVE
- ID:CVE-2022-3134CVE-2022-3153
- SUG:NA
- DESC:fix CVE-2022-3134 CVE-2022-3153

* Thu Sep 08 2022 renhongxun <renhongxun@h-partners.com> - 2:9.0-12
- Type:CVE
- ID:CVE-2022-3099
- SUG:NA
- DESC:fix CVE-2022-3099

* Sat Sep 03 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-11
- Type:CVE
- ID:CVE-2022-3037
- SUG:NA
- DESC:fix CVE-2022-3037

* Mon Aug 29 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-10
- Type:CVE
- ID:CVE-2022-3016
- SUG:NA
- DESC:fix CVE-2022-3016

* Sat Aug 27 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-9
- Type:CVE
- ID:CVE-2022-2980 CVE-2022-2982
- SUG:NA
- DESC:fix CVE-2022-2980 CVE-2022-2982

* Sat Aug 27 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable check

* Sat Aug 20 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-7
- Type:CVE
- ID:CVE-2022-2889 CVE-2022-2923 CVE-2022-2946
- SUG:NA
- DESC:fix CVE-2022-2889 CVE-2022-2923 CVE-2022-2946

* Thu Aug 18 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-6
- Type:CVE
- ID:CVE-2022-2816 CVE-2022-2817 CVE-2022-2819 CVE-2022-2845 CVE-2022-2849 CVE-2022-2862 CVE-2022-2874
- SUG:NA
- DESC:fix CVE-2022-2816 CVE-2022-2817 CVE-2022-2819 CVE-2022-2845 CVE-2022-2849 CVE-2022-2862 CVE-2022-2874

* Wed Aug 17 2022 wangjiang <wangjiang37@h-partners.com> - 2:9.0-5
- Type:bugfix
- ID:CVE-2022-2580 CVE-2022-2581
- SUG:NA
- DESC:fix CVE-2022-2580 CVE-2022-2581

* Wed Aug 10 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix failed to source defaults.vim

* Tue Aug 02 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-3
- Type:CVE
- ID:CVE-2022-2598 CVE-2022-2571
- SUG:NA
- DESC:fix CVE-2022-2598 CVE-2022-2571

* Sat Jul 30 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-2
- Type:CVE
- ID:CVE-2022-2257 CVE-2022-2264 CVE-2022-2284 CVE-2022-2285 CVE-2022-2286 CVE-2022-2287 CVE-2022-2288 CVE-2022-2289 CVE-2022-2304 CVE-2022-2343 CVE-2022-2344 CVE-2022-2345 CVE-2022-2522
- SUG:NA
- DESC:fix CVE-2022-2257 CVE-2022-2264 CVE-2022-2284 CVE-2022-2285 CVE-2022-2286 CVE-2022-2287 CVE-2022-2288 CVE-2022-2289 CVE-2022-2304 CVE-2022-2343 CVE-2022-2344 CVE-2022-2345 CVE-2022-2522

* Tue Jul 05 2022 shixuantong <shixuantong@h-partners.com> - 2:9.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade version to 9.0

* Thu Jun 23 2022 liukuo <liukuo@kylinos.cn> - 2:8.2-39
- Type:CVE
- ID:CVE-2022-2124
- SUG:NA
- DESC:fix CVE-2022-2124

* Tue Jun 21 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:8.2-38
- Type:CVE
- ID:CVE-2022-1771
- SUG:NA
- DESC:fix CVE-2022-1771

* Tue May 31 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-37
- Type:CVE
- ID:CVE-2022-1733 CVE-2022-1735
- SUG:NA
- DESC:fix CVE-2022-1733 CVE-2022-1735

* Sat May 21 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-36
- Type:CVE
- ID:CVE-2022-1629 CVE-2022-1620 CVE-2022-1674 CVE-2022-1621 CVE-2022-1619
- SUG:NA
- DESC:fix CVE-2022-1629 CVE-2022-1620 CVE-2022-1674 CVE-2022-1621 CVE-2022-1619

* Mon May 16 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-35
- Type:CVE
- ID:CVE-2022-1154
- SUG:NA
- DESC:fix CVE-2022-1154

* Mon May 09 2022 shangyibin <shangyibin1@h-partners.com> - 2:8.2-34
- Type:CVE
- ID:CVE-2022-1616
- SUG:NA
- DESC:fix CVE-2022-1616

* Thu Mar 24 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-33
- Type:CVE
- ID:CVE-2022-0943
- SUG:NA
- DESC:fix CVE-2022-0943

* Mon Mar 21 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-32
- Type:CVE
- ID:CVE-2022-0319 CVE-2022-0554
- SUG:NA
- DESC:fix CVE-2022-0319 CVE-2022-0554

* Sat Mar 19 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-31
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:cve patch test cases are rectified

* Wed Mar 09 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-30
- Type:CVE
- ID:CVE-2022-0685
- SUG:NA
- DESC:fix CVE-2022-0685

* Sat Mar 05 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-29
- Type:CVE
- ID:CVE-2022-0714 CVE-2022-0729
- SUG:NA
- DESC:fix CVE-2022-0714 CVE-2022-0729

* Mon Feb 28 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-28
- Type:CVE
- ID:CVE-2022-0629
- SUG:NA
- DESC:fix CVE-2022-0629

* Fri Feb 25 2022 yuanxin <yuanxin24@h-partners.com> - 2:8.2-27
- Type:CVE
- ID:CVE-2022-0572
- SUG:NA
- DESC:fix CVE-2022-0572

* Thu Feb 10 2022 tianwei <tianwei12@h-partners.com> - 2:8.2-26
- Type:CVE
- ID:CVE-2022-0443 CVE-2022-0392 CVE-2022-0417
- SUG:NA
- DESC:fix CVE-2022-0443 CVE-2022-0392 CVE-2022-0417

* Mon Feb 07 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-25
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix patches error

* Mon Feb 07 2022 shixuantong <shixuantong@h-partners.com> - 2:8.2-24
- Type:CVE
- ID:CVE-2022-0351 CVE-2022-0361 CVE-2022-0408 CVE-2022-0359 CVE-2022-0368 CVE-2022-0413
- SUG:NA
- DESC:fix CVE-2022-0351 CVE-2022-0361 CVE-2022-0408 CVE-2022-0359 CVE-2022-0368 CVE-2022-0413

* Sun Jan 30 2022 yuanxin<yuanxin24@h-partners.com> - 2:8.2-23
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix garbled characters display when file name matches

* Sat Jan 29 2022 guozhaorui<guozhaorui1@h-partners.com> - 2:8.2-22
- Type:CVE
- ID:CVE-2022-0213 CVE-2022-0261 CVE-2022-0318
- SUG:NA
- DESC:fix CVE-2022-0213 CVE-2022-0261 CVE-2022-0318

* Mon Jan 17 2022 yuanxin<yuanxin24@huawei.com> - 2:8.2-21
- Type:CVE
- ID:CVE-2021-4166 CVE-2021-4192 CVE-2021-4193
- SUG:NA
- DESC:fix CVE-2021-4166 CVE-2021-4192 CVE-2021-4193

* Thu Jan 13 2022 shixuantong<shixuantong@huawei.com> - 2:8.2-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build fail

* Sat Dec 11 2021 yuanxin<yuanxin24@huawei.com> - 2:8.2-19
- Type:CVE
- ID:CVE-2021-4069
- SUG:NA
- DESC:fix CVE-2021-4069

* Tue Dec 07 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-18
- Type:CVE
- ID:CVE-2021-4019
- SUG:NA
- DESC:fix CVE-2021-4019

* Sat Dec 04 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-17
- Type:CVE
- ID:CVE-2021-3984
- SUG:NA
- DESC:fix CVE-2021-3984

* Tue Nov 30 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-16
- Type:CVE
- ID:CVE-2021-3973 CVE-2021-3974
- SUG:NA
- DESC:fix CVE-2021-3973 CVE-2021-3974

* Sat Nov 27 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build fail for python3-3.10.0

* Sat Nov 13 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-14
- Type:CVE
- ID:CVE-2021-3927 CVE-2021-3927
- SUG:NA
- DESC:fix CVE-2021-3927 CVE-2021-3928

* Sat Oct 30 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-13
- Type:CVE
- ID:CVE-2021-3903
- SUG:NA
- DESC:fix CVE-2021-3903

* Sat Oct 23 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-12
- Type:CVE
- ID:CVE-2021-3872 CVE-2021-3875
- SUG:NA
- DESC:fix CVE-2021-3872 CVE-2021-3875

* Sun Sep 26 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-11
- Type:CVE
- ID:CVE-2021-3778 CVE-2021-3796
- SUG:NA
- DESC:fix CVE-2021-3778 CVE-2021-3796

* Sat Sep 11 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-10
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2021-3770

* Tue Aug 10 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix signal stack size is wrong with latest glibc 2.34

* Sat Aug 07 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix configure does not recognize gcc 10.0 and later

* Sat Jun 12 2021 shixuantong<shixuantong@huawei.com> - 2:8.2-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add gcc to BuildRequires

* Mon Nov 2 2020 wangjie<wangjie294@huawei.com> -2:8.2-6
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove python2

* Wed Oct 14 2020 Jie Yang<yangjieyj.yang@huawei.com> - 2:8.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix wrong path passed to --with-python3-config-dir.

* Sat Aug 15 2020 shixuantong<shixuantong@huawei.com> - 2:8.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix build failuers with perl 5.32

* Fri Aug 7 2020 wenzhanli<wenzhanli2@huawei.com> - 2:8.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix make test because lua update version 5.4.0

* Mon Jun 15 2020 chengquan<chengquan3@huawei.com> -2:8.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix compilation errors caused by python upgrade

* Thu Apr 16 2020 chengquan<chengquan3@huawei.com> -2:8.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v8.2

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-8
- update virc/vimrc to make escape work in insert mode

* Thu Jan 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-7
- make vi/vim  easy to uses

* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-6
- fix bug in install files

* Sat Jan 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-5
- make vi easy to use

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-4
- update spec and patch

* Sun Dec 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:8.1.450-3
- update vimrc/virc info

* Mon Sep 23 2019 huzhiyu<huzhiyu1@huawei.com> - 2:8.1.450-2
- Package init
